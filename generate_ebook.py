# coding=utf-8

"""
Learning Python the Hard Way e-book creator
    Copyright (C) 2014  JaPy Szoftver Kft

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
E_BOOK_URL = "http://learnpythonthehardway.org/book/"
from bs4 import BeautifulSoup, Comment
from urllib2 import urlopen
from epub_creator import Book, Chapter, ToC, create_epub
import argparse

def extract_toc():
    """
Extracts the Table of Contents of the e-book.
    :return: the extracted URLs and the Titles of the Table of Contents
    """
    soup = BeautifulSoup(urlopen(E_BOOK_URL))
    hrefs = soup.select("ul.simple > li > a")
    titles = [a.text for a in hrefs]
    urls = [a['href'] for a in hrefs]
    return urls, titles


def prettify_url_contents(url):
    """
Prettifies the HTML contents of a URL. This is needed because the E-Book site has some nasty </br> tags instead of
    <br/> tags. And this makes the parsing and element-finding a pain in the neck.
    :param url: the URL of the site to prettify
    :return:
    """
    soup = BeautifulSoup(urlopen(url))
    return BeautifulSoup(soup.prettify())


def extract_content_page(url):
    """
Extracts the contents of the e-book chapter avoiding the menu and the advertisements.
    :param url: the chapter part of the E-Book URL.
    """
    print "Scraping ", url
    soup = prettify_url_contents(E_BOOK_URL + url)
    main_div = soup.find("div", id="main")

    h4s = main_div.find_all('h4')
    for h4 in h4s:
        h4.name = 'h5'
    h3s = main_div.find_all('h3')
    for h3 in h3s:
        h3.name = 'h4'
    h2s = main_div.find_all('h2')
    for h2 in h2s:
        h2.name = 'h3'
    h1s = main_div.find_all('h1')
    for h1 in h1s:
        h1.name = 'h2'

    tts = main_div.find_all('tt')
    for tt in tts:
        tt.name = 'code'

    for chw in main_div.find_all(class_="chapters-wrapper"):
        chw.decompose()
    for vh in main_div.find_all(id="video_holder"):
        vh.decompose()

    comments = main_div.findAll(text=lambda text: isinstance(text, Comment))
    [comment.extract() for comment in comments]

    alist = main_div.find_all('a')
    for a in alist:
        del(a['name'])

    cols = main_div.find_all('col')
    for col in cols:
        if 'width' in col.attrs:
            style = "width: %s;" % col['width']
            if 'style' in col.attrs:
                style += col['style']
            col['style'] = style
            del(col['width'])

    theads = main_div.find_all('thead')
    for thead in theads:
        if 'valign' in thead.attrs:
            style = "vertical-align: %s;" % thead['valign']
            if 'style' in thead.attrs:
                style += thead['style']
            thead['style'] = style
            del(thead['valign'])
    tbodys = main_div.find_all('tbody')
    for tbody in tbodys:
        if 'valign' in tbody.attrs:
            style = "vertical-align: %s;" % tbody['valign']
            if 'style' in tbody.attrs:
                style += tbody['style']
            tbody['style'] = style
            del(tbody['valign'])

    figs = main_div.select('div.figure')
    for fig in figs:
        fig.decompose()

    res = ""
    for s in str(main_div).split('\n'):
        if len(s.strip()) != 0:
            res += s + '\n'

    return res


def parse_site():
    """
This method parses the URL containing the e-book
    """
    print "Scraping Table of Contents"
    urls, titles = extract_toc()
    print "Scraping %d chapters" % len(urls)
    chapters = []
    for i in range(len(urls)):
        chapters.append(Chapter(extract_content_page(urls[i]), urls[i], titles[i]))

    return Book(ToC(titles, urls), chapters,
                'Learn Python the Hard Way: A Very Simple Introduction to the Terrifyingly Beautiful World of ' +
                'Computers and Code, Third Edition (Jason Arnold\'s Library)', "Zed A. Shaw",
                "Copyright (C) 2014 Zed A. Shaw", "2014")


if __name__ == '__main__':
    print
    print "Learning Python the Hard Way e-book creator, Copyright (C) 2014 JaPy Szoftver Kft"
    print "This e-book creator comes with ABSOLUTELY NO WARRANTY."
    print "This is free software, and you are welcome to redistribute it under certain conditions."
    print "For more details read the LICENSE file."
    print

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", help="The output path to create the e-book.", required=True)
    parser.add_argument("-f", "--file", help="""The name of the output file -- without ending. For example
        'learning_python' -- the created file would be 'learning_python.epub""", required=True)
    args = parser.parse_args()

    create_epub(args.path, args.file, parse_site())


