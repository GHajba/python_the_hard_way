This is a simple web-scraper to load the book
http://learnpythonthehardway.org/book/
and create an e-book from the contents.

I initiated this project because I like to read books on my iPad or Kindle and not actually with live HTML connection.
And I dislike annoying advertisement.

Another motivation was the price of the e-book: $20 - $30 is a bit too much for an e-book and because the HTML version
is available on-line why not get it and convert it to an e-book to carry on your favourite reader?

## Requirements
The project is a Python 2.7 application. It could work with newer versions but I tested it only with 2.7.5

To install the requirements for this project execute
    
    pip install -r requirements.txt

The requirements include:

 * beautifulsoup4
 * lxml

## Usage
The main file is the **generate_ebook.py**. It requires two parameters: the output path and the filename without extension:

    python generate_ebook.py -p your/path/here -f learning_python

## Copyright
As you can see, this application is licensed under the GNU GPL v2. So you can redistribute this application

## Features
Currently the application is barely good enough: it has some hard-coded points, it contains only the text: no images, no css. 
And some parts are not well formed: the line numbering is not aligned correctly. The references of *Appendix A* are missing.

However it can be opened with iBooks (the nearest ePub reader I can test my results on) 
although the ePub validator misses some files in the archive. 

For the future I plan:

 * add the missing *Appendix A*
 * to add support for the images and after that I probably add css to fix the layout problems of the epub.
 * to make the ebpub_creator more flexible enabling reuse for other e-books not only Learning Python the Hard Way.