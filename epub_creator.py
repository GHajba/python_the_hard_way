# coding=utf-8

"""
epub creator
    Copyright (C) 2014  JaPy Szoftver Kft

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

import os
from lxml import etree
import shutil
from datetime import datetime


class Book:

    def __init__(self, toc, chapters, title, author="Unknown", rights="None", year="2014"):
        self.toc = toc
        self.chapters = chapters
        self.title = title
        self.author = author
        self.rights = rights
        self.year = year


class Chapter:
    def __init__(self, content, filename, title):
        self.content = content
        self.filename = filename
        self.title = title


class ToC:

    def __init__(self, titles, filenames):
        self.titles = titles
        self.filenames = filenames

OPF_NAMESPACE = "http://www.idpf.org/2007/opf"
DC_NAMESPACE = "http://purl.org/dc/elements/1.1/"
OPS_NAMESPACE = "http://www.idpf.org/2007/ops"
DC = "{%s}" % DC_NAMESPACE


#TODO: make identifier more unique
#TODO: make description a parameter
#TODO: make language a parameter


def create_container(output_path):
    nsmap = {None: "urn:oasis:names:tc:opendocument:xmlns:container"}
    root = etree.Element("container", version="1.0", nsmap=nsmap)
    rootfiles = etree.SubElement(root, "rootfiles")
    rootfile = etree.SubElement(rootfiles, "rootfile")
    rootfile.set("full-path", "content.opf")
    rootfile.set("media-type", "application/oebps-package+xml")
    f = open(output_path + "/tmp/META-INF/" + "container.xml", 'w')
    f.write(etree.tostring(root, pretty_print=True, xml_declaration=True, encoding='utf-8'))
    f.flush()
    f.close()


def create_content_opf(output_path, book):
    nsmap = {None: OPF_NAMESPACE, "dc": DC_NAMESPACE, "opf": OPF_NAMESPACE}
    root = etree.Element("package", version="3.0", nsmap=nsmap)
    root.set("unique-identifier", "LPHW9780133124347")
    metadata = etree.SubElement(root, "metadata")
    item = etree.SubElement(metadata, DC + "identifier")
    item.set("id", "LPHW9780133124347")
    item.text = "LPHW9780133124347"
    lang = etree.SubElement(metadata, DC + "language")
    lang.text = "en"
    title = etree.SubElement(metadata, DC + "title")
    title.attrib['{http://www.w3.org/XML/1998/namespace}lang'] = "en"
    title.text = book.title
    creator = etree.SubElement(metadata, DC + "creator")
    creator.text = book.author
    created = etree.SubElement(metadata, DC + "date")
    created.text = book.year
    rights = etree.SubElement(metadata, DC + "rights")
    rights.text = book.rights
    meta = etree.SubElement(metadata, "meta", property="dcterms:modified")
    meta.text = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
    manifest = etree.SubElement(root, "manifest")
    item = etree.SubElement(manifest, "item", id="nav", href="nav.xhtml", properties="nav")
    item.set("media-type", "application/xhtml+xml")
    spine = etree.SubElement(root, "spine")
    #spine = etree.SubElement(root, "spine", toc="ncx")
    #spine.set("page-map", "map")
    spine.append(etree.Element("itemref", idref="nav"))

    for file in book.toc.filenames:
        id = file[:-5]
        item = etree.SubElement(manifest, "item", id=id, href=file)
        item.set("media-type", "application/xhtml+xml")
        spine.append(etree.Element("itemref", idref=id))


    f = open(output_path+"/tmp/content.opf", "w")
    f.write(etree.tostring(root, pretty_print=True, xml_declaration=True, encoding='utf-8'))
    f.flush()
    f.close()


def cleanup(output_path):
    shutil.rmtree(output_path+"/tmp")


def create_toc(output_path, toc):
    print "Creating 'Table of Contents'"
    root = etree.Element("html", nsmap={None: "http://www.w3.org/1999/xhtml", "ops": OPS_NAMESPACE})
    root.attrib['{http://www.w3.org/XML/1998/namespace}lang'] = "en"
    head = etree.SubElement(root, "head")
    title = etree.SubElement(head, "title")
    title.text = "Table of Contents"
    body = etree.SubElement(root, "body")
    nav = etree.SubElement(body, "nav")
    nav.attrib['{' + OPS_NAMESPACE + '}type'] = "toc"
    h1 = etree.SubElement(nav, "h1")
    h1.text = "Table of Contents"
    ol = etree.SubElement(nav, "ol")
    li = etree.SubElement(ol, "li")
    a = etree.SubElement(li, "a", href="nav.xhtml")
    a.text = "Table of Contents"
    for i in range(len(toc.filenames)):
        li = etree.SubElement(ol, "li")
        a = etree.SubElement(li, "a", href=toc.filenames[i])
        a.text = toc.titles[i]
    f = open(output_path + "/tmp/nav.xhtml", "w")
    f.write(etree.tostring(root, pretty_print=True, xml_declaration=True, encoding='utf-8'))
    f.flush()
    f.close()


def create_toc_ncx(output_path, book):
    root = etree.Element("ncx", version="2005-1", nsmap={None: "http://www.daisy.org/z3986/2005/ncx/"})
    root.attrib['{http://www.w3.org/XML/1998/namespace}lang'] = "en"
    head = etree.SubElement(root, "head")
    head.append(etree.Element("meta", name="dtb:uid", content="LPHW9780133124347"))
    docTitle = etree.SubElement(root, "docTitle")
    txt = etree.SubElement(docTitle, "text")
    txt.text = book.title
    navMap = etree.SubElement(root, "navMap")
    for i in range(len(book.toc.filenames)):
        navPoint = etree.SubElement(navMap, "navPoint", id=("navPoint%d"%(i+1)), playOrder="i+1")


def create_section(content, body):
    tree = etree.fromstring(content)
    for e in tree.xpath("/div[1]/*"):
        body.append(e)


def create_content(output_path, chapters):
    for chapter in chapters:
        print "Creating '%s'" % chapter.title
        root = etree.Element("html", nsmap={None: "http://www.w3.org/1999/xhtml", "ops": OPS_NAMESPACE})
        root.attrib['{http://www.w3.org/XML/1998/namespace}lang'] = "en"
        head = etree.SubElement(root, "head")
        title = etree.SubElement(head, "title")
        title.text = chapter.title
        body = etree.SubElement(root, "body")
        create_section(chapter.content, body)
        f = open(output_path + "/tmp/%s" % chapter.filename, "w")
        f.write(etree.tostring(root, pretty_print=True, xml_declaration=True, encoding='utf-8'))
        f.flush()
        f.close()


def compress_files(output_path, epub_name):
    shutil.make_archive(output_path+'/'+epub_name, format='zip', root_dir=output_path+'/tmp')
    os.rename(output_path+'/' + epub_name + '.zip', output_path+'/' + epub_name + '.epub')


def create_mimetype(output_path):
    f = open(output_path + "/tmp/mimetype", "w")
    f.write("application/epub+zip")
    f.flush()
    f.close()


def create_epub(output_path, epub_name, book):
    """
Creates the epub to the given path with the given name. It creates a version 3 epub file.
    :param output_path: the location where to create the epub file
    :param epub_name: the name of the epub file to be generated
    :param book: the book to export as epub
    """
    create_folder_structure(output_path)
    create_mimetype(output_path)
    create_container(output_path)
    create_content_opf(output_path, book)
    create_toc(output_path, book.toc)
    create_toc_ncx(output_path, book)
    create_content(output_path, book.chapters)
    compress_files(output_path, epub_name)
    cleanup(output_path)


def create_folder_structure(output_path):
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    os.mkdir(output_path + "/tmp")  # temp folder to put the data
    os.mkdir(output_path + "/tmp/META-INF")